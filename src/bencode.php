<?php

if (!function_exists('bencode')) {
    function bencode($value): string
    {
        if (is_scalar($value)) {
            // If $value is an integer or a string containing an integer
            // is_numeric() would accept float type which is not allowed.
            if (ctype_digit(strval($value))) {
                return "i{$value}e";

            // if $value is not an integer but is still a scalar, we can handle it
            // as a string
            } else {
                return strlen($value).':'.$value;
            }
            // Is it a numeric array or an associative array ?
        } elseif (array_keys($value) === range(0, count($value) - 1)) {
            return 'l'.implode('', array_map('bencode', $value)).'e';
        } else {
            ksort($value);

            $r = implode(
                '',
                array_map(
                    function ($a, $b) {
                        return bencode($a).bencode($b);
                    },
                    array_keys($value),
                    array_values($value)
                )
            );

            return "d{$r}e";
        }
    }
}
