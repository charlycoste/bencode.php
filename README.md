bencode.php
===========

What is bencode.php ?
---------------------

bencode.php is a lightweight recursive function for benconding data in PHP.
As it's purely functional, it's a bit faster than other libraries but others
are good too.


Installation
------------

    composer require coste/bencode


Examples
--------

bencode.php acts as a simple polyfill. If you don't have a global 'bencode'
function, then you will get one now…

    <?php

    require_once 'vendor/autoload.php';

    echo bencode([
        'hello',
        42,
        [
            'a' => [1, 2, 3],
            'b' => 12.42
        ]
    ]);

Licence
-------

[![AGPL](http://www.gnu.org/graphics/agplv3-155x51.png)](http://www.gnu.org/licenses/agpl-3.0.html)

Copyright (C) 2020 Charles-Édouard Coste

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
